Rails.application.routes.draw do
  resource 'devices', only: [:index]

  root 'devices#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
