class AddDeviceToEvent < ActiveRecord::Migration[5.0]
  def change
    add_reference :events, :device, foreign_key: true
  end
end
