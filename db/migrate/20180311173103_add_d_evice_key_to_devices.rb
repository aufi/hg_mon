class AddDEviceKeyToDevices < ActiveRecord::Migration[5.0]
  def change
    add_column :devices, :device_key, :string
  end
end
