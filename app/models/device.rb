# == Schema Information
#
# Table name: devices
#
#  id          :integer          not null, primary key
#  name        :string
#  device_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  device_key  :string
#

class Device < ApplicationRecord
  has_many :events, dependent: :destroy
  validates :device_key, presence: true
end
