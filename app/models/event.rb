# == Schema Information
#
# Table name: events
#
#  id           :integer          not null, primary key
#  event_type   :string
#  name         :string
#  value        :float
#  origin       :string
#  raw          :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  device_id_id :integer
#  device_id    :integer
#

class Event < ApplicationRecord
  belongs_to :device
  validates :event_type, presence: true
  validates :device,     presence: true

  # TODO: move to separate class
  def self.monitor_irc(opts = {})
    bot = Cinch::Bot.new do
    configure do |c|
      c.server = opts['server'] || "192.168.7.2"
      c.nick   = "mon_#{`hostname -f`}"
      c.channels = ["#general"]
    end

    #on :connect do
    #  Channel('#general').send 'abilities'
    #end
    #JSON.dump(msg_type: "ability", name: "relay.podsviceni_tv", type: "switch", title: "Podsvícení za televizí")

    on :message do |m|
      event_hash = Hash.new
      begin
        event_hash = JSON.parse(m.message)
      rescue JSON::ParserError => ex
        puts "No JSON format"
        p ex
        return false
      end
      return false unless event_hash['msg_type'] == 'status'
      device = Device.find_or_create_by(device_key: event_hash['name'])
      Event.create! device: device, event_type: 'status', name: event_hash['name'], value: event_hash['value'], raw: event_hash
    end
end

bot.start
  end
end
